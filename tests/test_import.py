# -*- coding: utf-8 -*-
############################################################################
# Copyright (C) 2018  Internet Systems Consortium, Inc. ("ISC")
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# See the COPYRIGHT file distributed with this work for additional
# information regarding copyright ownership.
############################################################################

import os
import sys
import unittest
sys.path.insert(0,
                os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
                )
import rndc

# A simple sample test that doesn't do anything useful, yet.
class TestImportRNDC(unittest.TestCase):
    def test_import_rndc(self):
        self.assertEqual(1, 1)
