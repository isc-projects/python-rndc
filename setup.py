"""
Python RNDC protocol library

A native python library that implements the RNDC protocol.
"""
from codecs import open
from os import path
from setuptools import setup
from unittest import TestLoader

from rndc import __version__

here = path.abspath(path.dirname(__file__))

with open(path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()


def get_test_suite():
    test_loader = TestLoader()
    test_suite = test_loader.discover('tests', pattern='test_*.py')
    return test_suite


setup(
    name="rndc",
    version=__version__,
    description="RNDC Protocol Library",
    long_description=long_description,
    long_description_content_type='text/markdown',

    keywords="library DNS RNDC BIND",

    author="Internet Systems Consortium",
    license="MPL 2.0",
    url="https://gitlab.isc.org/isc-projects/python-rndc",

    classifiers=[
        'Development Status :: 4 - Beta',
        'Intended Audience :: Developers',
        'Natural Language :: English',
        'Operating System :: POSIX',
        'Operating System :: Unix',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2.7',
        'Topic :: Internet :: Name Service (DNS)',
        'Topic :: Software Development :: Libraries',
        'License :: OSI Approved :: Mozilla Public License 2.0 (MPL 2.0)',
    ],

    test_suite='setup.get_test_suite',
    py_modules=['rndc'],
    install_requires=[],

    data_files=[("", ["COPYRIGHT"])]

)
